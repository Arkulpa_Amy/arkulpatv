module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      keyframes: {
        background: {
          '0%, 100%': { backgroundPosition: '0% 50%' },
          '50%': { backgroundPosition: '100% 50%' },
        },
        roll: {
          "0%,100%": {transform:'rotate(0deg)'},
          "25%":{transform:"rotate(15deg)"},
          "50%":{transform:'rotate(20deg)'}
        },
        shiver: {
          '0%, 100%': { 
            transform: 'translate(5px, 0) rotate(0deg)' 
          },
          '25%': { 
            transform: 'translate(-10px, 2px) rotate(12deg)' 
          },
          '50%': { 
            transform: 'translate(10px, -2px) rotate(0deg)' 
          },
          '75%': { 
            transform: 'translate(-10px, -2px) rotate(-12deg)' 
          },
        },
      },
      animation: {
        border: 'background ease infinite',
        shiver: 'shiver 0.5s infinite',
        'shiver-slow': 'shiver 8.0s infinite',
        planet:'roll  4s linear infinite',
      },
    },
    
    colors: {
      "blue-zomp": "#4DA394",
      "brown-caput-mortuum": "#59322B",
      "beige-buttercream": "#FFFCC7",
      "orange-cognac": "#EF9A48",
      "red-rusty-red":"#D54751",
      "off-black": "#303030",
      "off-white": "#f0f0f0",
      "zuckerpink": "#ffedf7",
      "zuckerpink-dunkel":"#c01c45",
      "waldgrün-hell": "#8b9c86",
      "waldgrün-mittel": "#3f5a36",
      "nachtblau":"#01012f",
      "zwielichtblau":"#588cc3",
      "asphalt":"#525257",
      "himmelschwarz":"#101022",
      "pflaume": "#6c296a",
      "blackel":"#000000",
      "büroboden": "#85a1b9",
      "bürodecke": "#95bfe5"
    },
  },
  variants: {

    extend: {},
  },
  
  plugins: [],
};
