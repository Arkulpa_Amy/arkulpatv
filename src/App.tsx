import React from "react";
import {BrowserRouter as Router, Route, Link, Routes} from "react-router-dom";

import BugsNBolts from "./minigames/Bugs'n'Bolts";
import HeidiWife from "./minigames/HeidiWife";
import MaisRunner from "./minigames/MaisRunner";
import WhackAScreen from "./minigames/Whack-A-Screen";
import Shokoshooter from "./minigames/SCHoKoLaDe";
import WalknerWalker from "./minigames/WalknerDrivner";
import Schrumpelpong from "./minigames/Schrumpelpong";
import OliveOrder from "./minigames/OliveOrder";
import PumperJumper from "./minigames/PumperJumper";

const MainMenu = () => {
  return (
    <div className='flex flex-col items-center justify-center h-screen bg-gray-100'>
      <h1 className='text-3xl font-bold mb-8'>Main Menu</h1>
      <ul className='list-none'>
        <li className='mb-4'>
          <Link to='/bugs-n-bolts' className='text-blue-500 hover:underline'>
            Bugs 'n' Bolts
          </Link>
        </li>
        <li className='mb-4'>
          <Link to='/heidi-wifey' className='text-blue-500 hover:underline'>
            Heidi Wifey
          </Link>
        </li>
        <li className='mb-4'>
          <Link to='/mais-runner' className='text-blue-500 hover:underline'>
            Mais Runner
          </Link>
        </li>
        <li className='mb-4'>
          <Link to='/whack-a-screen' className='text-blue-500 hover:underline'>
            Whack-A-Screen
          </Link>
        </li>
        <li className='mb-4'>
          <Link to='/shokoshooter' className='text-blue-500 hover:underline'>
            Shokoshooter
          </Link>
        </li>
        <li className='mb-4'>
          <Link to='/walkner-walker' className='text-blue-500 hover:underline'>
            Walkner Walker
          </Link>
        </li>
        <li className='mb-4'>
          <Link to='/schrumpelpong' className='text-blue-500 hover:underline'>
            Schrumpelpong
          </Link>
        </li>
        <li className='mb-4'>
          <Link to='/olive-order' className='text-blue-500 hover:underline'>
            Olive Order
          </Link>
        </li>
        <li className='mb-4'>
          <Link to='/pumper-jumper' className='text-blue-500 hover:underline'>
            Pumper Jumper
          </Link>
        </li>
      </ul>
    </div>
  );
};

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<MainMenu />} />
        <Route path='/bugs-n-bolts' element={<BugsNBolts />} />
        <Route path='/heidi-wifey' element={<HeidiWife />} />
        <Route path='/mais-runner' element={<MaisRunner />} />
        <Route path='/whack-a-screen' element={<WhackAScreen />} />
        <Route path='/shokoshooter' element={<Shokoshooter />} />
        <Route path='/walkner-walker' element={<WalknerWalker />} />
        <Route path='/schrumpelpong' element={<Schrumpelpong />} />
        <Route path='/olive-order' element={<OliveOrder />} />
        <Route path='/pumper-jumper' element={<PumperJumper />} />
      </Routes>
    </Router>
  );
};

export default App;
