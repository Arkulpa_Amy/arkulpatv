import React, {useState, useEffect} from "react";

const OliveOrder = () => {
  const [missedCount, setMissedCount] = useState(0);
  const [isTimeToAnswer, setIsTimeToAnswer] = useState(false);
  const [isPhoneHidden, setIsPhoneHidden] = useState(true);
  const [elseHidden, setElseHidden] = useState(false);
  const [hours, setHours] = useState(12);
  const [minutes, setMinutes] = useState(30);
  const [gameStarted, setGameStarted] = useState(false);
  const [tippingMusicPlaying, setTippingMusicPlaying] = useState(false);
  const tippingAudio = new Audio("minigamessound/tipping.mp3");
  const bgAudio = new Audio("minigamessound/bgOlive.mp3");
  const failaudio = new Audio("/minigamessound/fail.mp3");
  const winaudio = new Audio("/minigamessound/success.mp3");

  useEffect(() => {
    if (gameStarted) {
      const tickInterval = setInterval(() => {
        setMinutes((prevMinutes) => (prevMinutes + 10) % 60);
        if (minutes === 50) {
          setHours((prevHours) => (prevHours + 1) % 24);
        }

        if (hours === 11 && minutes >= 0 && minutes <= 50) {
          setIsTimeToAnswer(true);
        } else {
          setIsTimeToAnswer(false);
        }
      }, 250);

      return () => clearInterval(tickInterval);
    }
  }, [hours, minutes, gameStarted]);

  useEffect(() => {
    if (!elseHidden && gameStarted) {
      const tippingTimeout = setTimeout(() => {
        setTippingMusicPlaying(true);
        tippingAudio
          .play()
          .catch((err) => console.log("Error playing audio: ", err));
      }, Math.floor(Math.random() * 5000) + 5000); // Random time between 5 to 10 seconds
      return () => clearTimeout(tippingTimeout);
    }
  }, [elseHidden, gameStarted]);

  const handleClockClick = () => {
    if (isTimeToAnswer) {
      setIsTimeToAnswer(false);
      setIsPhoneHidden(false);
      winaudio.play();

      setTimeout(() => {
        setIsPhoneHidden(true);
        setElseHidden(!elseHidden);
      }, 500);
    } else {
      failaudio.play();
      setMissedCount((prev) => prev + 1);
    }
  };

  const handleStartGame = () => {
    bgAudio.play().catch((err) => console.log("Error playing audio: ", err));
    setGameStarted(true);
  };

  return (
    <div draggable='false' className=' select-none overflow-clip'>
      {!gameStarted && (
        <div className='overflow-clip'>
          <p className='absolute top-[13.5%] place-content-center md:text-4xl left-[20%] h-[10%] text-center right-[20%] border border-orange-cognac p-5'>
            Welcome to Olive Order!
          </p>
          <p className='absolute top-[25%] place-content-center h-[11.5%] md:text-4xl text-center w-[60%] left-[20%] right-[20%] border border-orange-cognac p-5'>
            Klicke zur rechten Zeit auf die Uhr, um beim Olive essen zu
            bestellen!
          </p>
          <button
            onClick={handleStartGame}
            className='absolute md:text-5xl top-[38%] h-[10%] left-[20%] right-[20%] border border-orange-cognac p-5'
          >
            → Start Game ←
          </button>
        </div>
      )}
      {gameStarted && (
        <div className='overflow-clip'>
          <img
            draggable='false'
            alt='bürobg'
            src='/minigamesgraph/oliveBG.jpeg'
            className='scale-x-[-1] w-[100%] left-[0.5%] relative overflow-clip object-contain h-[100vh]'
          />
          <div className='bg-bürodecke w-screen md:h-[78%] h-[70%] top-[0%] -z-20 absolute overflow-clip'></div>

          <div className='bg-büroboden w-screen md:h-[23%] h-[45%] bottom-0 -z-20 absolute overflow-clip'></div>
          <div>
            {!elseHidden && (
              <div
                id='clock'
                onClick={handleClockClick}
                className='absolute top-10 bg-off-white border border-off-black rounded-md p-2 left-1/2 transform -translate-x-1/2 cursor-pointer'
              >
                <span>
                  {hours < 10 ? "0" + hours : hours}:
                  {minutes < 10 ? "0" + minutes : minutes}
                </span>
              </div>
            )}
          </div>
          {!elseHidden && (
            <div
              id='scoreBoard'
              className='absolute md:top-[10%] text-brown-caput-mortuum left-[48%]'
            >
              <p>
                Missed: <span>{missedCount}</span>
              </p>
            </div>
          )}
          <img
            alt='da ändi'
            src={
              !elseHidden
                ? `${"/minigamesgraph/andyNoSmile.png"}`
                : `${"/minigamesgraph/andySmile.png"}`
            }
            className='absolute w-[20%] md:w-[14%] md:h-[30%] md:left-[15.5%] md:top-[28.3%] left-[6%] top-[39.9%]'
          />
          {elseHidden && (
            <div>
              <img
                draggable='false'
                alt='essen'
                className='absolute z-20 md:w-[25.7%] md:h-[39%] md:top-[32.5%] md:left-[13%] top-[40%] w-[40%] h-[20%] left-[2%]'
                src='/minigamesgraph/essen.png'
              ></img>
              <p className='absolute left-[5%] right-[5%] overflow-clip z-40 top-10 text-7xl'>
                {"Mahlzeit, Bua!"}
              </p>
            </div>
          )}
          {!isPhoneHidden && (
            <img
              alt='telefon'
              src='/minigamesgraph/telefon.png'
              className='absolute md:left-[19%] md:w-20 md:h-20 w-[7%] h-[5%] left-[10%] top-[43%] md:top-[36%]'
            />
          )}
        </div>
      )}
    </div>
  );
};

export default OliveOrder;
