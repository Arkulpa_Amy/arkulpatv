import React, {useState, useEffect} from "react";

// Define an interface for obstacles to ensure TypeScript understands the structure of our obstacles array
interface Obstacle {
  imgsrc: string;
  x: number;
  y: number;
}

const PumperJumper = () => {
  const [avatarY, setAvatarY] = useState<number>(0);
  const [obstacles, setObstacles] = useState<Obstacle[]>([]);
  const [timer, setTimer] = useState<number>(0);
  const [score, setScore] = useState<number>(0);
  const [gameStarted, setGameStarted] = useState<boolean>(false);
  const bgMusic = new Audio("/minigamessound/neptume.mp3");

  useEffect(() => {
    if (gameStarted) {
      const timerId = setInterval(() => {
        setTimer((prevTimer) => prevTimer + 1);
        setScore((prevScore) => prevScore + 1);
      }, 1000);

      return () => clearInterval(timerId);
    }
  }, [gameStarted]);

  useEffect(() => {
    if (gameStarted) {
      const interval = setInterval(() => {
        setObstacles((prevObstacles) =>
          prevObstacles
            .map((obs) => ({...obs, x: obs.x - 8})) // Adjusted obstacle speed
            .filter((obs) => obs.x > -50)
        );
      }, 50 - score / 10);

      return () => clearInterval(interval);
    }
  }, [gameStarted]);

  useEffect(() => {
    if (gameStarted && timer % 3 === 0) {
      const obstacleType =
        Math.random() < 0.5 ? "greenheart.gif" : "orangeherz.gif";
      const newObstacle = {
        x: Math.random() * 250 + 950,
        y: Math.random() * 50 + Math.random() * 670,
        imgsrc: `/minigamesgraph/${obstacleType}`,
      };

      setObstacles((prevObstacles) => [...prevObstacles, newObstacle]);
    }
  }, [timer, gameStarted]);

  useEffect(() => {
    if (timer >= 250) {
      setGameStarted(false);
    }
  }, [timer]);

  const handleStartGame = () => {
    bgMusic.play().catch((err) => console.log("Error playing music", err));
    setGameStarted(true);
  };

  const handleMouseMove = (event: React.MouseEvent<HTMLDivElement>) => {
    if (gameStarted) {
      setAvatarY(event.clientY);
    }
  };

  useEffect(() => {
    obstacles.forEach((obs) => {
      const avatarCenterY = avatarY + 25;
      const obstacleCenterY = obs.y + 5;
      const distanceX = Math.abs(obs.x - 5 - 5);
      const distanceY = Math.abs(obstacleCenterY - avatarCenterY);
      const distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

      if (distance < 30) {
        setScore((prevScore) => prevScore - 10);
      }
    });
  }, [avatarY, obstacles]);

  return (
    <div
      className='relative h-screen overflow-clip select-none  cursor-none'
      onMouseMove={handleMouseMove}
    >
      {!gameStarted && (
        <div className='hover:cursor-pointer absolute inset-0 flex flex-col justify-center items-center bg-pflaume text-zuckerpink'>
          <h1 className='text-3xl font-bold mb-6'>Welcome to PumperJumper!</h1>
          <p className='text-lg mb-4'>Instructions:</p>
          <p className='text-lg mb-6'>
            Avoid Amys Pickup-Lines to increase your score up to 250 to win the
            game. Move your cursor up and down to control the character.
          </p>
          <button
            onClick={handleStartGame}
            className='border hover:bg-zuckerpink text-zuckerpink hover:text-pflaume font-bold py-2 px-4 rounded-full'
          >
            Start Game
          </button>
        </div>
      )}
      {gameStarted && (
        <div className='cursor-none'>
          <img
            alt='bg'
            src='/minigamesgraph/linusbg.gif'
            className=' cursor-none absolute -z-50 object-cover h-[105vh] w-screen overflow-clip'
          ></img>
          <div className=' cursor-none'>
            <div className='absolute' style={{top: avatarY}}>
              <img
                src='/minigamesgraph/linus.png'
                alt='linus'
                className='w-12 h-20'
              ></img>
            </div>
            {obstacles.map((obs, index) => (
              <img
                key={index}
                alt='Obstacle'
                src={obs.imgsrc}
                className='absolute w-10 h-10'
                style={{bottom: `${obs.y}px`, left: `${obs.x}px`}}
              ></img>
            ))}
            <span className='absolute top-2 z-50 text-off-white left-2 text-white'>
              Score: {score}
            </span>
          </div>
        </div>
      )}
    </div>
  );
};

export default PumperJumper;
