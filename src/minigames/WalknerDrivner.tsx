import {time} from "console";
import React, {useState, useEffect} from "react";

interface Position {
  x: number;
  y: number;
}

interface Obstacle extends Position {
  speed: number;
  rotation: number;
}

const gridSize = 10;

const createEmptyGrid = (): number[][] =>
  Array.from({length: gridSize}, () => Array(gridSize).fill(0));

const WalknerWalker = () => {
  const [walknerPosition, setWalknerPosition] = useState<Position>({
    x: 4,
    y: 9,
  });
  const [facingRight, setFacingRight] = useState<boolean>(false);
  const [obstacles, setObstacles] = useState<Obstacle[]>([]);
  const [timer, setTimer] = useState<number>(-50);
  const [isLanded, setIsLanded] = useState(false);
  const [freshGame, setFreshGame] = useState(true);

  useEffect(() => {
    const moveWalkner = (event: KeyboardEvent) => {
      const {x, y} = walknerPosition;
      switch (event.key) {
        case "ArrowUp":
          setWalknerPosition((prev) => ({...prev, y: Math.max(prev.y - 1, 0)}));
          break;
        case "ArrowDown":
          setWalknerPosition((prev) => ({
            ...prev,
            y: Math.min(prev.y + 1, gridSize - 1),
          }));
          break;
        case "ArrowLeft":
          setWalknerPosition((prev) => ({...prev, x: Math.max(prev.x - 1, 0)}));
          setFacingRight(true);
          break;
        case "ArrowRight":
          setWalknerPosition((prev) => ({
            ...prev,
            x: Math.min(prev.x + 1, gridSize - 1),
          }));
          setFacingRight(false);
          break;
        default:
          break;
      }
    };

    window.addEventListener("keydown", moveWalkner);
    return () => window.removeEventListener("keydown", moveWalkner);
  }, [walknerPosition]);

  const resetGame = () => {
    setWalknerPosition({x: 4, y: 9});
    setFacingRight(false);
    setObstacles([]);
    setTimer(0);
    setIsLanded(false);
    setFreshGame(false);
  };

  const generateObstacle = () => {
    const obstaclePositionX = Math.floor(Math.random() * gridSize);
    const rotation = -50;
    const newObstacle: Obstacle = {
      x: obstaclePositionX,
      y: 0,
      speed: 1,
      rotation,
    };
    setObstacles((prevObstacles) => [...prevObstacles, newObstacle]);
  };

  useEffect(() => {
    const obstacleGenerator = setInterval(generateObstacle, 380 - timer);
    return () => clearInterval(obstacleGenerator);
  }, [timer]);

  useEffect(() => {
    const checkCollision = () => {
      obstacles.forEach((obstacle) => {
        if (
          obstacle.x === walknerPosition.x &&
          obstacle.y === walknerPosition.y
        ) {
          setTimer((prevTimer) => prevTimer - 50);
        }
      });
    };

    const collisionCheck = setInterval(checkCollision, 100);
    return () => clearInterval(collisionCheck);
  }, [obstacles, walknerPosition]);

  useEffect(() => {
    const moveObstacles = () => {
      setObstacles((prevObstacles) =>
        prevObstacles
          .map((obstacle) => ({...obstacle, y: obstacle.y + obstacle.speed}))
          .filter((obstacle) => obstacle.y < gridSize)
      );
    };

    const gameLoop = setInterval(moveObstacles, 100);
    return () => clearInterval(gameLoop);
  }, [obstacles, timer]);

  useEffect(() => {
    const timerId = setInterval(() => {
      setTimer((prevTimer) => prevTimer + 5);
    }, 500);

    return () => clearInterval(timerId);
  }, []);

  useEffect(() => {
    if (timer > 320) {
      setObstacles([]);
      setIsLanded(true);
    }
  }, [isLanded, timer]);

  const backgroundImage = () => {
    if (timer <= 0) {
      return `/minigamesgraph/wüste.gif`;
    } else if (timer >= 1 && timer < 100) {
      return `/minigamesgraph/weltraum1.gif`;
    } else if (timer >= 100 && timer < 200) {
      return `/minigamesgraph/weltraum2.jpeg`;
    } else if (timer >= 200 && timer < 300) {
      return `/minigamesgraph/weltraum3.gif`;
    } else {
      return `/minigamesgraph/moon.gif`;
    }
  };

  const obstacleImage = () => {
    if (timer <= 0) {
      return "/minigamesgraph/tumbleweed.png";
    } else if (timer >= 0 && timer < 100) {
      return `/minigamesgraph/wolke.png`;
    } else if (timer >= 100 && timer < 200) {
      return `/minigamesgraph/meteor.png`;
    } else {
      return `/minigamesgraph/sternschnupfe.gif`;
    }
  };

  return (
    <div className='h-screen bg-blackel select-none relative overflow-hidden'>
      <div className='w-20'></div>
      <img
        alt='bg'
        src={`${backgroundImage()}`}
        className='absolute opacity-80 h-[100vh] w-screen object-cover'
      />

      {freshGame && !isLanded && (
        <div className=' w-screen h-[100vh] bg-off-black absolute z-40'>
          <img
            alt='1'
            className='border md:w-fit border-red-rusty-red h-[9%] w-[45%] absolute z-50 p-4 md:left-[70%] left-[28%] md:right-[20%] bottom-[18%] -rotate-3'
            src='/minigamesgraph/walknerchen.png'
          ></img>
          <img
            alt='2'
            className='border md:w-fit border-red-rusty-red  absolute z-50 h-[8%] w-[45%] p-2 left-[26%] md:left-[70%] md:right-[20%] bottom-[8.5%] -rotate-6'
            src='/minigamesgraph/mondfahrt.png'
          ></img>

          <p className='text-blackel bg-red-rusty-red p-3 w-[70%] absolute z-20 bottom-[60%] text-6xl text-center left-[15%] right-[15%]'>
            Wollen wir zum Mond radeln?
          </p>

          <div className=''>
            <button
              className='text-blackel bg-red-rusty-red p-4 w-[20%] left-[40%] top-[42%] right-[40%] absolute z-20 text-2xl text-center'
              onClick={() => resetGame()}
            >
              Ja!
            </button>
          </div>

          <div className=''>
            <p className='md:text-3xl text-off-white md:w-60 right-[10%] left-[10%] md:right-[17%] absolute bottom-[32%] text-xl text-center md:bottom-0 md:top-[65%]'>
              Vermeiden Sie alle Hindernisse auf dem Radweg zum Mond.
              <>
                <br />
                <br />
              </>
              Steuerung:
              <br />↑ ↓ → ←
            </p>
          </div>
        </div>
      )}

      {isLanded && !freshGame && (
        <div className='w-screen bg-blackel'>
          <img
            alt='1'
            className='border md:w-fit hidden md:flex bg-red-rusty-red border-red-rusty-red h-[7%] w-[45%] absolute z-50 p-4 md:left-[4%] left-[1%] top-[9%] -rotate-3'
            src='/minigamesgraph/walknerchen.png'
          ></img>
          <img
            alt='2'
            className='border md:w-fit hidden md:flex bg-red-rusty-red border-red-rusty-red absolute z-50 h-[8%] w-[45%] p-2 right-[5%] md:left-[6%] top-[18%] -rotate-6'
            src='/minigamesgraph/mondfahrt.png'
          ></img>
          <img
            className='bg-blackel absolute bottom-0 w-[100%] md:left-20 md:right-20 h-[100vh] object-contain z-20'
            alt='SuccGraph'
            src='/minigamesgraph/walknerMond.jpeg'
          />
          <p className='text-blackel bg-red-rusty-red p-3 w-[100%] md:w-[70%] absolute z-20 bottom-10 text-6xl text-center md:left-[20%] md:right-[20%]'>
            Wilkommen auf dem Mond, Stefan.
          </p>
        </div>
      )}

      {!freshGame && !isLanded && (
        <div className='grid grid-cols-10 gap-1 h-[90vh] relative w-screen z-10'>
          {createEmptyGrid().map((row, rowIndex) =>
            row.map((cell, cellIndex) => (
              <div
                key={`${rowIndex}-${cellIndex}`}
                className='w-full h-full flex justify-center items-center'
              >
                {rowIndex === walknerPosition.y &&
                cellIndex === walknerPosition.x ? (
                  <img
                    src='/minigamesgraph/walkner.gif'
                    alt='Walkner'
                    className={`w-15 h-28 object-cover absolute z-30 ${
                      facingRight ? "scale-x-[-1]" : ""
                    }`}
                  />
                ) : null}
                {obstacles.some(
                  (obstacle) =>
                    obstacle.x === cellIndex && obstacle.y === rowIndex
                ) && (
                  <img
                    src={`${obstacleImage()}`}
                    alt='Obstacle'
                    style={{
                      transform: `rotate(${
                        obstacles.find(
                          (obstacle) =>
                            obstacle.x === cellIndex && obstacle.y === rowIndex
                        )?.rotation
                      }deg)`,
                    }}
                    className='w-20 h-30 object-cover absolute'
                  />
                )}
              </div>
            ))
          )}
        </div>
      )}
    </div>
  );
};

export default WalknerWalker;
