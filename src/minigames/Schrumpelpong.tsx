/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useRef} from "react";

interface Face {
  imgSrc: string;
}

const Schrumpelpong = () => {
  const gameAreaRef = useRef<HTMLDivElement>(null);
  const [paddle, setPaddle] = useState({width: 100, height: 20, x: 0, y: 0});
  const [ball, setBall] = useState({x: 50, y: 50, radius: 20, dx: 2, dy: -2});
  const [gameSize, setGameSize] = useState({width: 800, height: 400});
  const [isExplosion, setIsExplosion] = useState(false);
  const [timer, setTimer] = useState(0);
  const [face, setFace] = useState<Face>({imgSrc: "/minigamesgraph/face4.png"});
  const [menu, setMenu] = useState(true);

  const resetGame = () => {
    setIsExplosion(false);
    setPaddle({width: 100, height: 20, x: 0, y: 0});
    setBall({x: 50, y: 50, radius: 20, dx: 2, dy: -2});
    setTimer(0);
  };

  useEffect(() => {
    if (gameAreaRef.current) {
      setPaddle({
        ...paddle,
        x: gameAreaRef.current.offsetWidth / 2 - paddle.width / 2,
        y: gameAreaRef.current.offsetHeight - paddle.height,
      });
      setBall({
        ...ball,
        x: gameAreaRef.current.offsetWidth / 2,
        y: gameAreaRef.current.offsetHeight / 2,
      });
    }
  }, [gameAreaRef.current]);

  useEffect(() => {
    const handleResize = () => {
      setGameSize({
        width: gameAreaRef.current?.offsetWidth ?? 1000,
        height: gameAreaRef.current?.offsetHeight ?? 400,
      });
    };

    window.addEventListener("resize", handleResize);
    handleResize();

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    let intervalId: any;
    if (!isExplosion && !menu) {
      intervalId = setInterval(() => {
        setTimer((prevTimer) => prevTimer + 1);
      }, 1000); // Increase the timer by 1 second (1000 milliseconds)
    }

    return () => clearInterval(intervalId);
  }, [isExplosion, menu]);

  useEffect(() => {
    let intervalId: NodeJS.Timeout;
    if (!isExplosion && !menu) {
      intervalId = setInterval(() => {}, 7 - timer / 10);
    }

    return () => clearInterval(intervalId);
  }, [isExplosion, timer, menu]);

  useEffect(() => {
    if (ball.y >= window.innerHeight * 0.8) {
      setFace({imgSrc: "/minigamesgraph/face3.png"});
    } else if (ball.y < 3 * (window.innerHeight / 10)) {
      setFace({imgSrc: "/minigamesgraph/face1.png"});
    } else if (ball.y < 3 * (window.innerHeight / 5)) {
      setFace({imgSrc: "/minigamesgraph/face2.png"});
    } else if (menu !== true) {
      setFace({imgSrc: "/minigamesgraph/face2.png"});
    }
  }, [ball.y, menu]);

  const handleMouseMove = (e: React.MouseEvent<HTMLDivElement>) => {
    if (gameAreaRef.current) {
      const gameAreaRect = gameAreaRef.current.getBoundingClientRect();
      const newX = e.clientX - gameAreaRect.left - paddle.width / 2;
      setPaddle((prevPaddle) => ({...prevPaddle, x: newX}));
    }
  };

  const startGame = () => {
    setMenu(false);
  };

  useEffect(() => {
    const updateGame = () => {
      if (menu !== true) {
        let newX = ball.x + ball.dx;
        let newY = ball.y + ball.dy;

        if (newX < ball.radius || newX > gameSize.width - ball.radius) {
          setBall((prevBall) => ({...prevBall, dx: -prevBall.dx}));
        }

        if (newY < ball.radius) {
          setBall((prevBall) => ({...prevBall, dy: -prevBall.dy}));
        }

        if (
          newY > gameSize.height - paddle.height - ball.radius &&
          newX > paddle.x &&
          newX < paddle.x + paddle.width
        ) {
          setBall((prevBall) => ({...prevBall, dy: -prevBall.dy}));

          setPaddle((prevPaddle) => ({
            ...prevPaddle,
            width: prevPaddle.width * 0.9,
          }));
        }

        setBall((prevBall) => ({...prevBall, x: newX, y: newY}));

        if (newY > gameSize.height) {
          setIsExplosion(true);
        }
      }
    };

    const gameInterval = setInterval(updateGame, 7 - timer / 10);
    return () => clearInterval(gameInterval);
  }, [ball, paddle, gameSize]);

  return (
    <div>
      <div>
        {menu && (
          <div className='absolute overflow-clip md:mt-[0%] mt-[20%] md:h-[100vh] h-[70vh] gap-1 bg-off-white w-[100%] z-50'>
            <div>
              <p className='text-3xl md:mt-[10%] md:ml-[10%]'>
                {
                  "Das ist Alex. Alex muss die pittoreske Welt retten, indem er eine Minute lang die A-Bombe balanciert."
                }
              </p>
              <button
                className='border w-fit mt-3 p-2 absolute md:left-[40%] px-5 border-zwielichtblau text-zwielichtblau'
                onClick={startGame}
              >
                {"Go?"}
              </button>

              <img
                alt='pfeil'
                className='w-[22%] md:w-[10%] mt-3 md:-mt-[10%] rotate-[80deg] md:absolute md:top-[30%]'
                src='/minigamesgraph/pfeil.png'
              ></img>

              <div>
                <img
                  alt='alex'
                  className='absolute top-[42%] md:top-[30%] bottom-[30%] left-[2.5%] z-20 w-[20%] h-auto'
                  src='/minigamesgraph/alex.png'
                ></img>
                <img
                  alt='gesicht'
                  className='absolute z-30 left-[5.5%] md:top-[49%] top-[51%] rotate-6 bottom-[40%] w-[14%]'
                  src='/minigamesgraph/face4.png'
                ></img>
              </div>
            </div>
            <div className='absolute text-3xl right-[20%]'>
              <p>{"Steuerung:"}</p>
              <img
                alt='mausPikto'
                className='w-auto h-[15%]'
                src='/minigamesgraph/maus.png'
              ></img>
            </div>
          </div>
        )}
      </div>

      {/* Render this area when the timer is higher than 200 */}
      {timer >= 6000 && (
        <div className='w-screen h-[100vh] bg-waldgrün-hell absolute z-50'>
          <img
            alt='planet'
            src='/minigamesgraph/planet.png'
            className='h-auto w-[20%] absolute left-[10%] top-[25%] animate-planet'
          />
          <p className="absolute z-30 top-[20%] left-[35%] text-9xl text-off-black'>">
            You did it. You saved the world. Congratulations!
          </p>
          <button
            className='absolute z-30 top-[54%] left-[27%] border border-off-black p-2'
            onClick={() => {
              resetGame();
            }}
          >
            Try again
          </button>
        </div>
      )}

      {!isExplosion && timer <= 60 && !menu && (
        <div
          className='absolute z-30 text-off-black
         top-0 left-0 p-4'
        >
          Time: {timer} seconds
        </div>
      )}

      {isExplosion ? (
        <div>
          <div className='absolute left-[30%] right-[30%] md:top-0 top-[20%]'>
            <p className='absolute z-30 text-4xl md:text-9xl top-28 w-full -left-10 text-off-black'>
              Game Over.
            </p>
            <p className='absolute z-30 top-40 md:top-96 md:-left-5 -left-10 text-4xl md:text-6xl text-off-black'>
              Well, obviously.
            </p>
            <button
              className='absolute z-30 md:left-96 -left-20 ml-10 border md:top-64 top-44 mt-10 border-off-black p-2'
              onClick={() => {
                resetGame();
              }}
            >
              Try again?
            </button>
          </div>
          <img
            alt='explosion'
            src='/gifs/explosion.gif'
            className='w-screen h-[100vh] object-cover overflow-clip absolute z-20'
          />
        </div>
      ) : (
        <div className='cursor-none'>
          <div>
            <img
              alt='alex'
              className='absolute top-[42%] md:top-[30%] bottom-[30%] left-[2.5%] z-20 w-[10%] h-auto'
              src='/minigamesgraph/alex.png'
            ></img>
            <img
              alt='gesicht'
              className='absolute z-30 left-[4%] md:top-[40%] top-[45%] bottom-[40%] w-[7%]'
              src={face.imgSrc}
            ></img>
          </div>
          <img
            alt='hintergrund'
            src='/minigamesgraph/wiese.gif'
            className='w-screen object-cover h-[100vh] absolute overflow-hidden'
          />
          <div
            ref={gameAreaRef}
            onMouseMove={handleMouseMove}
            className='overflow-clip relative h-[100vh] w-screen'
          >
            <img
              alt='ball'
              className='absolute rounded-full'
              src='/minigamesgraph/bomberl.png'
              style={{
                top: `${ball.y - ball.radius}px`,
                left: `${ball.x - ball.radius}px`,
                width: `${ball.radius * 3}px`,
                height: `${ball.radius * 3}px`,
              }}
            />
            {!menu && (
              <img
                alt='paddle'
                className='absolute bottom-0 border-x mb-2 border-x-zuckerpink'
                src='/minigamesgraph/blitzle.gif'
                style={{
                  left: paddle.x,
                  width: paddle.width,
                  height: paddle.height,
                }}
              />
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default Schrumpelpong;
