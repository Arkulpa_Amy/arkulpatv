import React, {useState, useEffect, useRef} from "react";
import boardData from "./map.json";

const MaisRunner = () => {
  const [board, setBoard] = useState<number[][]>(boardData.initialBoard);
  const initialPlayerPosition = {x: 1, y: 1};
  const [playerPosition, setPlayerPosition] = useState(initialPlayerPosition);
  const [prevPosition, setPrevPosition] = useState(initialPlayerPosition);
  const [deersCaught, setDeersCaught] = useState(-1);
  const intervalRef = useRef<NodeJS.Timeout | null>(null);
  const [musicButton, setMusicButton] = useState(false);
  const shotGun = "/minigamessound/shotgun.mp3";
  const backgroundMusic = "/minigamessound/KILLER.mp3";
  const [showMenu, setShowMenu] = useState(true);

  const playSound = (path: string) => {
    new Audio(path).play().catch((error) => console.log(error, path));
  };

  const imageSourcesScreen = [
    "/minigamesgraph/tally1.png",
    "/minigamesgraph/tally2.png",
    "/minigamesgraph/tally3.png",
    "/minigamesgraph/tally4.png",
    "/minigamesgraph/tally5.png",
  ];
  interface Position {
    oldY: number;
    oldX: number;
    newY: number;
    newX: number;
  }

  useEffect(() => {
    if (musicButton) {
      playSound(backgroundMusic);
      setShowMenu(false);
    }
  }, [musicButton]);

  useEffect(() => {
    const initializedBoard = boardData.initialBoard.map((row, rowIndex) =>
      row.map((cell, cellIndex) =>
        rowIndex === initialPlayerPosition.y &&
        cellIndex === initialPlayerPosition.x
          ? 2
          : cell
      )
    );
    setBoard(initializedBoard);

    if (board[initialPlayerPosition.y][initialPlayerPosition.x] === 2) {
      const newBoard = [...board];
      newBoard[initialPlayerPosition.y][initialPlayerPosition.x] = 1;
      setBoard(newBoard);
    }
  }, [board, initialPlayerPosition.x, initialPlayerPosition.y]);

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      const {x, y} = playerPosition;
      let newX = x;
      let newY = y;

      switch (e.key) {
        case "ArrowUp":
          newY -= 1;
          break;
        case "ArrowDown":
          newY += 1;
          break;
        case "ArrowLeft":
          newX -= 1;
          break;
        case "ArrowRight":
          newX += 1;
          break;
        default:
          return;
      }

      newY = Math.max(0, Math.min(newY, board.length - 1));
      newX = Math.max(0, Math.min(newX, board[0].length - 1));

      if (board[newY][newX] === 1 || board[newY][newX] === 3) {
        let updatedBoard = board.map((row) => [...row]);
        if (board[newY][newX] === 3) {
          setDeersCaught(deersCaught + 1);
          try {
            playSound(shotGun);
          } catch (error) {
            console.log(error);
          }
        }
        updatedBoard[playerPosition.y][playerPosition.x] = 1;

        updatedBoard[newY][newX] = 2;
        setBoard(updatedBoard);
        setPrevPosition(playerPosition);
        setPlayerPosition({x: newX, y: newY});
      }
    };
    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, [playerPosition, board, deersCaught, shotGun]);

  useEffect(() => {
    const newBoard = board.map((row) => [...row]);

    if (newBoard[prevPosition.y][prevPosition.x] === 2) {
      newBoard[prevPosition.y][prevPosition.x] = 1;
    }

    newBoard[playerPosition.y][playerPosition.x] = 2;
    setBoard(newBoard);
  }, [playerPosition, prevPosition, board]);

  useEffect(() => {
    const moveDeers = () => {
      const newBoard = board.map((row) => [...row]);

      const newDeerPositions: Position[] = [];
      board.forEach((row, rowIndex) => {
        row.forEach((cell, cellIndex) => {
          if (cell === 3) {
            const possibleMoves = [
              {x: -1, y: 0}, // Left
              {x: 1, y: 0}, // Right
              {x: 0, y: -1}, // Up
              {x: 0, y: 1}, // Down
            ];

            const validMoves = possibleMoves.filter((move) => {
              const newY = rowIndex + move.y;
              const newX = cellIndex + move.x;
              return (
                newY >= 0 &&
                newY < board.length &&
                newX >= 0 &&
                newX < row.length &&
                board[newY][newX] === 1
              );
            });

            if (validMoves.length > 0) {
              const move =
                validMoves[Math.floor(Math.random() * validMoves.length)];
              const newY = rowIndex + move.y;
              const newX = cellIndex + move.x;

              // Queue the deer's new position for update
              newDeerPositions.push({
                oldY: rowIndex,
                oldX: cellIndex,
                newY,
                newX,
              });
            }
            if (!intervalRef.current) {
              intervalRef.current = setInterval(moveDeers, 100);
            }

            return () => {
              if (intervalRef.current) {
                clearInterval(intervalRef.current);
                intervalRef.current = null;
              }
            };
          }
        });
      });

      // Update the board with new deer positions
      newDeerPositions.forEach((pos) => {
        newBoard[pos.oldY][pos.oldX] = 1; // Reset old position to path
        newBoard[pos.newY][pos.newX] = 3; // Move deer to new position
      });

      setBoard(newBoard); // Update the board state
    };
    let randomTime = Math.random() * 10;

    const interval = setInterval(moveDeers, randomTime); // Move deers every other second
    return () => clearInterval(interval);
  }, [board]);

  useEffect(() => {
    const spawnDeers = () => {
      const newBoard = [...board];
      let clearPositions = [];

      for (let y = 0; y < newBoard.length; y++) {
        for (let x = 0; x < newBoard[y].length; x++) {
          if (newBoard[y][x] === 1) {
            clearPositions.push({y, x});
          }
        }
      }

      if (clearPositions.length > 0) {
        const numNewDeers = Math.min(
          clearPositions.length,
          Math.floor(Math.random() * 3) + 1
        );
        for (let i = 0; i < numNewDeers; i++) {
          const deerPosition = clearPositions.splice(
            Math.floor(Math.random() * clearPositions.length),
            1
          )[0];
          newBoard[deerPosition.y][deerPosition.x] = 3;
        }
        setBoard(newBoard);
      }
    };

    const interval = setInterval(spawnDeers, 20);
    return () => clearInterval(interval);
  }, [board]);

  const renderBoard = () =>
    board.map((row, rowIndex) => (
      <div className='flex md:ml-60 max-w-screen' key={rowIndex}>
        {row.map((cell, cellIndex) => (
          <img
            alt='content'
            key={cellIndex}
            className={`md:w-10 w-6 md:h-10 h-6
            ${cell === 0 ? "bg-waldgrün-mittel" : ""}
            ${cell === 2 ? "bg-red-rusty-red" : ""}
            ${cell === 3 ? "bg-red-rusty-red" : ""}
            ${cell === 1 ? "bg-beige-buttercream" : ""}
          `}
            src={`
            ${cell === 0 ? "/minigamesgraph/spirelli.jpeg" : ""}
            ${cell === 2 ? "/minigamesgraph/steph.png" : ""}
            ${cell === 3 ? "/minigamesgraph/reh.png" : ""}
          `}
          />
        ))}
      </div>
    ));

  const startGame = () => {
    setMusicButton(true);
    setDeersCaught(0);
  };

  return (
    <div className='w-screen overflow-clip h-[100vh] bg-waldgrün-hell flex justify-center items-center flex-col'>
      {showMenu && (
        <div className='absolute overflow-clip bg-waldgrün-hell h-screen w-screen flex flex-col justify-center items-center'>
          <img
            alt='steph'
            src='/minigamesgraph/steph.png'
            className='md:w-[18%] w-[25%] absolute top-5 left-[27%]'
          ></img>
          <img
            alt='pfeil'
            src='/minigamesgraph/pfeil.png'
            className='-rotate-[30deg] absolute left-[15%] w-[10%] top-[12%]'
          ></img>
          <div className='absolute text-5xl top-0 md:top-[10%] left-[10%] mt-52'>
            <p className=''>Hey Stef. Dis you </p>
            <div className='h-60 md:h-30' />
            <p className=''>Dis your target. Kill a lot of them: </p>
          </div>
          <img
            alt='pfeil'
            src='/minigamesgraph/pfeil.png'
            className='rotate-[10deg] absolute left-[15%] w-[10%] top-[65%]'
          ></img>
          <img
            alt='reh'
            src='/minigamesgraph/reh.png'
            className='w-[20%] absolute top-[66%] left-[22%]'
          ></img>
          <div className='absolute hidden md:flex md:flex-col right-[10%] -mt-52'>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
          </div>

          <button
            onClick={startGame}
            className='bg-red-rusty-red text-white absolute left-[10%] top-[38%] md:top-[47%] font-bold py-2 px-4 rounded-full focus:outline-none focus:shadow-outline hover:bg-red-rusty-red-light'
          >
            Start Game
          </button>
        </div>
      )}
      {(deersCaught > 14 || deersCaught === 100) && (
        <div className='absolute bg-red-rusty-red overflow-clip h-screen w-screen animate-pulse opacity-96'>
          <div className='hidden md:flex absolute right-[10%] -mt-52'>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
            <img
              alt='killer'
              src='/minigamesgraph/KIIIIIILLER.webp'
              className='md:ml-5 ml-28 pl-20 mt-15'
            ></img>
          </div>
          <img
            alt='knopf'
            src='/minigamesgraph/again.png'
            className='md:pt-72 md:pl-20 hover:cursor-pointer'
            onClick={startGame}
          ></img>
        </div>
      )}
      {renderBoard()}
      {deersCaught > 0 && (
        <img
          src={
            imageSourcesScreen[
              Math.min(deersCaught - 1, imageSourcesScreen.length - 1)
            ]
          }
          alt='Tally'
          className='md:mt-4 absolute md:left-5 md:right-0 md:bottom-[30%] md:top-[80%] right-48 top-2'
        />
      )}
      {deersCaught > 5 && (
        <img
          src={
            imageSourcesScreen[
              Math.min(deersCaught - 6, imageSourcesScreen.length - 1)
            ]
          }
          alt='Tally'
          className='md:mt-4 absolute md:left-5 right-28 rotate-2 top-3 md:top-[70%] md:bottom-10'
        />
      )}
      {deersCaught > 10 && (
        <img
          src={
            imageSourcesScreen[
              Math.min(deersCaught - 11, imageSourcesScreen.length - 1)
            ]
          }
          alt='Tally'
          className='md:mt-4 absolute md:left-[8%] right-5 top-1 md:bottom-10 md:top-[75%]'
        />
      )}
    </div>
  );
};

export default MaisRunner;
