import React, {useState, useEffect} from "react";

interface Schoki {
  id: number;
  y: number;
  fromX: number;
}

interface Enemy {
  id: number;
  x: number;
  y: number;
  src: string;
  isActive: boolean;
}

const imageSourcesEnemies = [
  "/minigamesgraph/andy.png",
  "/minigamesgraph/emi.png",
  "/minigamesgraph/domi.png",
  "/minigamesgraph/walkner.gif",
];

const audioSourceSchoko = [
  "/minigamessound/schokisound1.mp3",
  "/minigamessound/schokisound2.mp3",
  "/minigamessound/schokisound3.mp3",
  "/minigamessound/schokisound4.mp3",
];

const Shokoshooter = () => {
  const [playerPosition, setPlayerPosition] = useState({x: 500, y: 10});
  const [schokis, setSchokis] = useState<Schoki[]>([]);
  const [enemies, setEnemies] = useState<Enemy[]>([]);
  const [direction, setDirection] = useState<string>("right");
  const [score, setScore] = useState(0);
  const [gameStarted, setGameStarted] = useState(false);

  const shootSchoko = () => {
    const newSchoki: Schoki = {
      id: Date.now(),
      y: playerPosition.y,
      fromX: playerPosition.x,
    };
    setSchokis((prevSchokis) => [...prevSchokis, newSchoki]);
  };

  const handleStartGame = () => {
    setGameStarted(true);
  };

  const playRandomAudio = () => {
    const randomIndex = Math.floor(Math.random() * audioSourceSchoko.length);
    const audio = new Audio(audioSourceSchoko[randomIndex]);
    audio.play();
  };

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (gameStarted) {
        if (e.key === "ArrowLeft") {
          setPlayerPosition((prev) => ({
            ...prev,
            x: Math.max(prev.x - 50, 350),
          }));
        } else if (e.key === "ArrowRight") {
          setPlayerPosition((prev) => ({
            ...prev,
            x: Math.min(prev.x + 50, window.innerWidth - 350),
          }));
        } else if (e.key === " ") {
          shootSchoko();
        }
      }
    };

    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, [playerPosition, gameStarted]);

  useEffect(() => {
    if (gameStarted) {
      const createEnemy = () => {
        const src =
          imageSourcesEnemies[
            Math.floor(Math.random() * imageSourcesEnemies.length)
          ];
        const startPosition = -50;
        setEnemies((prevEnemies) => [
          ...prevEnemies,
          {
            id: Date.now(),
            src,
            x: startPosition,
            y: 100,
            isActive: true,
          },
        ]);
      };

      const intervalId = setInterval(createEnemy, 1200);
      return () => clearInterval(intervalId);
    }
  }, [gameStarted]);

  useEffect(() => {
    if (gameStarted) {
      const moveEnemies = setInterval(() => {
        setEnemies((prevEnemies) =>
          prevEnemies.map((enemy) => {
            let newX = enemy.x + (direction === "right" ? 5 : -5);
            if (newX >= window.innerWidth - 10 || newX <= 0) {
              setDirection((prevDirection) =>
                prevDirection === "right" ? "left" : "right"
              );
            }
            return {...enemy, x: newX};
          })
        );
      }, 50);

      return () => clearInterval(moveEnemies);
    }
  }, [direction, gameStarted]);

  useEffect(() => {
    if (gameStarted) {
      const moveSchoki = setInterval(() => {
        setSchokis((prevSchokis) =>
          prevSchokis
            .map((schoki) => ({
              ...schoki,
              y: schoki.y + 3,
            }))
            .filter((schoki) => schoki.y < window.innerHeight)
        );
      }, 5);

      return () => clearInterval(moveSchoki);
    }
  }, [gameStarted]);

  useEffect(() => {
    if (gameStarted) {
      const checkCollisions = () => {
        setSchokis((prevSchokis) => {
          let newSchokis = [...prevSchokis];
          let newEnemies = [...enemies];

          for (let i = 0; i < newSchokis.length; i++) {
            const schoki = newSchokis[i];
            for (let j = 0; j < newEnemies.length; j++) {
              const enemy = newEnemies[j];
              const hitX =
                schoki.fromX >= enemy.x && schoki.fromX <= enemy.x + 40;
              const hitY =
                schoki.y + 10 >= enemy.y && schoki.y - 10 <= enemy.y + 40;
              if (hitX && hitY) {
                newEnemies[j].isActive = false;
                newSchokis.splice(i, 1);
                newEnemies.splice(j, 1);
                setScore((prevScore) => prevScore + 1);
                if (score % 5 === 0) {
                  playRandomAudio();
                }
                i--;
                break;
              }
            }
          }

          return newSchokis;
        });

        setEnemies((prevEnemies) => [...prevEnemies]);
      };

      const collisionInterval = setInterval(checkCollisions, 10);

      return () => clearInterval(collisionInterval);
    }
  }, [enemies, gameStarted, score]);

  return (
    <div className=' h-screen w-screen relative overflow-clip'>
      {!gameStarted && (
        <div className='text-brown-caput-mortuum absolute inset-0 flex flex-col justify-center items-center bg-brown-caput-mortuum bg-opacity-35 text-white'>
          <h1 className='text-3xl font-bold mb-6'>
            {"Shokoshooter - Stopf deine Lämmchen mit Schokolade voll!"}
          </h1>
          <h1 className='text-3xl font-bold mb-6'>
            {"Steuerung: → ←  und Leertaste"}
          </h1>
          <button
            onClick={handleStartGame}
            className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
          >
            Start Game
          </button>
        </div>
      )}
      {gameStarted && (
        <div>
          <img
            alt='thomas'
            src='/minigamesgraph/thomas.png'
            className='bg-waldgrün-hell absolute bottom-5 w-[5%] h-auto'
            style={{
              left: `${playerPosition.x}px`,
              bottom: `${playerPosition.y}px`,
            }}
          ></img>

          <div className='bg-waldgrün-mittel h-96'>
            {enemies.map(
              (enemy) =>
                enemy.isActive && (
                  <img
                    key={enemy.id}
                    src={enemy.src}
                    className='absolute w-[40px] h-[40px] bg-waldgrün-hell'
                    style={{
                      left: `${enemy.x}px`,
                      top: `${enemy.y}px`,
                    }}
                    alt='Enemy'
                  />
                )
            )}
          </div>
          <p className='text-zuckerpink'>Score: {score}</p>
          {schokis.map((schoki) => (
            <img
              alt='schoki'
              src='/minigamesgraph/schoko.png'
              key={schoki.id}
              className='absolute w-[40px] h-[40px] ml-6 bg-beige-buttercream'
              style={{
                left: `${schoki.fromX}px`,
                bottom: `${schoki.y}px`,
              }}
            />
          ))}
        </div>
      )}
    </div>
  );
};

export default Shokoshooter;
