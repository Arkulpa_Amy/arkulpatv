import React, {useState, useEffect, useRef} from "react";

interface Bolt {
  id: number;
  x: number;
  fromY: number;
}

interface Bug {
  id: number;
  x: number;
  y: number;
  isActive: boolean;
  speed: number;
}

const BugsNBolts = () => {
  const [playerPosition, setPlayerPosition] = useState({x: 50, y: 50});
  const [bolts, setBolts] = useState<Bolt[]>([]);
  const [bugs, setBugs] = useState<Bug[]>([]);
  const [clearCounter, setClearCounter] = useState(0);
  const [failCounter, setFailCounter] = useState(0);
  const [timer, setTimer] = useState(0);
  const [menu, setMenu] = useState(false);
  const playerPositionRef = useRef(playerPosition);

  const resetGame = () => {
    setBugs([]);
    setPlayerPosition({x: 50, y: 50});
    setClearCounter(0);
    setFailCounter(0);
    setTimer(0);
    setMenu(true);
  };

  useEffect(() => {
    let intervalId: any;
    intervalId = setInterval(() => {
      setTimer((prevTimer) => prevTimer + 1);
    }, 1000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    const spawnBug = () => {
      const newBug = {
        id: Date.now(),
        x: window.innerWidth - 100,
        y: Math.random() * (window.innerHeight - 50),
        isActive: true,
        speed: Math.random() * 3 + 1,
      };
      setBugs((prevBugs) => [...prevBugs, newBug]);
    };

    const bugSpawner = setInterval(spawnBug, 2000);
    return () => clearInterval(bugSpawner);
  }, []);

  useEffect(() => {
    playerPositionRef.current = playerPosition;
  }, [playerPosition]);

  useEffect(() => {
    const spawnInterval = Math.max(5000 - clearCounter * 50, 500);
    const spawnBug = () => {
      const newBug = {
        id: Date.now(),
        x: window.innerWidth - 100,
        y: Math.random() * (window.innerHeight - 50),
        isActive: true,
        speed: Math.random() * 3 + 1 + clearCounter * 0.1,
      };
      setBugs((prevBugs) => [...prevBugs, newBug]);
    };

    const bugSpawner = setInterval(spawnBug, spawnInterval);
    return () => clearInterval(bugSpawner);
  }, [clearCounter]);

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key !== "ArrowUp" && e.key !== "ArrowDown") {
        const newBolt: Bolt = {
          id: Date.now(),
          x: playerPositionRef.current.x,
          fromY: playerPositionRef.current.y + 60,
        };
        setBolts((prevBolts) => [...prevBolts, newBolt]);
      }
    };

    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, []);

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === "ArrowUp") {
        setPlayerPosition((prev) => ({...prev, y: Math.max(prev.y - 50, -10)}));
      } else if (e.key === "ArrowDown") {
        setPlayerPosition((prev) => ({
          ...prev,
          y: Math.min(prev.y + 50, window.innerHeight - 120),
        }));
      }
    };

    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, []);

  useEffect(() => {
    const moveBolts = setInterval(() => {
      setBolts((prevBolts) =>
        prevBolts
          .map((bolt) => ({
            ...bolt,
            x: bolt.x + 10,
          }))
          .filter((bolt) => bolt.x < window.innerWidth)
      );
    }, 5);

    return () => clearInterval(moveBolts);
  }, []);

  useEffect(() => {
    const checkCollisions = () => {
      setBolts((prevBolts) =>
        prevBolts.filter((bolt) => {
          let boltActive = true;
          setBugs((prevBugs) =>
            prevBugs.map((bug) => {
              if (
                boltActive &&
                bolt.x >= bug.x &&
                bolt.x <= bug.x + 60 &&
                bolt.fromY >= bug.y &&
                bolt.fromY <= bug.y + 60 &&
                bug.isActive &&
                timer <= 60
              ) {
                boltActive = false;
                setClearCounter(
                  (prevCount) => prevCount + Math.floor(Math.random() * 10)
                );
                return {...bug, isActive: false};
              }
              return bug;
            })
          );
          return boltActive;
        })
      );
    };

    const collisionCheckInterval = setInterval(checkCollisions, 10);
    return () => clearInterval(collisionCheckInterval);
  }, [timer]);

  useEffect(() => {
    const moveBugs = setInterval(() => {
      setBugs((currentBugs) =>
        currentBugs
          .map((bug) => ({...bug, x: bug.x - bug.speed}))
          .filter((bug) => {
            const leftScreenEdge = -100;
            if (bug.x < leftScreenEdge && bug.isActive && timer <= 60) {
              setFailCounter((prevCount) => prevCount + 1);
              return false;
            }
            return true;
          })
      );
    }, 50);

    return () => clearInterval(moveBugs);
  }, [timer]);

  return (
    <div className='select-none'>
      {timer < 60 && menu !== true && (
        <div className=''>
          <div className='absolute w-screen h-[50vh] left-0 top-0 z-50 bg-nachtblau text-zwielichtblau text-2xl md:text-4xl'>
            <p className='w-[70%] max-h-[10%] absolute top-[5%] bottom-[45%] left-[20%]'>
              {
                "Oh großer Cyber-Schamane! Der Stamm wird von bösartigen Bugs überfallen! Bitte helft uns!"
              }
            </p>

            <p className='w-[70%] max-h-[10%] absolute md:top-[35%] bottom-[55%] md:bottom-[65%] left-[20%]'>
              {
                "Vernichte so viele einfallende Bugs wie nur möglich. Steuerung: ↑ ↓ & any"
              }
            </p>
            <button
              className='bottom-[20%] absolute border border-zwielichtblau w-[26%] left-[42%] right-[42%]'
              onClick={resetGame}
            >
              {"Und los!"}
            </button>
          </div>

          <div className='w-screen overflow-clip h-[50vh] absolute bottom-0 z-10 bg-zwielichtblau'>
            <img
              className='w-max h-max absolute -top-10 -right-72 rotate-12'
              alt='text'
              src='/minigamesgraph/blitz2.gif'
            ></img>
            <img
              className='w-screen absolute top-[40%] opacity-50 z-40 px-20 animate-shiver-slow'
              alt='text'
              src='/minigamesgraph/cybershaman.png'
            ></img>
            <img
              className='w-max h-max absolute -top-10 -left-72'
              alt='text'
              src='/minigamesgraph/blitz2.gif'
            ></img>
            <img
              className='w-max h-max absolute -top-10 -left-40 rotate-180'
              alt='text'
              src='/minigamesgraph/blitz2.gif'
            ></img>
          </div>
        </div>
      )}

      {timer > 60 && (
        <div className='w-screen h-[100vh] absolute z-10'>
          <div className='absolute w-screen h-[50vh] left-0 top-[0] bg-nachtblau text-zwielichtblau text-5xl md:text-6xl'>
            <div className='p-2 top-[5%] max-w-[50%] left-[25%] right-[25%] absolute'>
              <p>Score: {clearCounter - failCounter}</p>
              {clearCounter - failCounter <= 0 && (
                <p>Die Bugs haben diesmal leider gewonnen.</p>
              )}
              {clearCounter - failCounter > 1 &&
                clearCounter - failCounter < 500 && (
                  <p>Immerhin hast du ein paar Bugs erwischt.</p>
                )}

              {clearCounter - failCounter > 500 && (
                <p>Du bist ein wahrer Buggerschreck.</p>
              )}
              <button
                className='border animate-pulse p-1'
                onClick={() => resetGame()}
              >
                Nochmal?
              </button>
            </div>
          </div>
          <div className='w-screen overflow-clip h-[50vh] absolute bottom-0 z-10 bg-zwielichtblau'>
            <img
              className='w-max h-max absolute -top-10 -right-72 rotate-12'
              alt='text'
              src='/minigamesgraph/blitz2.gif'
            ></img>
            <img
              className='w-screen absolute top-[40%] opacity-50 z-40 px-20 animate-shiver-slow'
              alt='text'
              src='/minigamesgraph/cybershaman.png'
            ></img>
            <img
              className='w-max h-max absolute -top-10 -left-72'
              alt='text'
              src='/minigamesgraph/blitz2.gif'
            ></img>
            <img
              className='w-max h-max absolute -top-10 -left-40 rotate-180'
              alt='text'
              src='/minigamesgraph/blitz2.gif'
            ></img>
          </div>
        </div>
      )}

      <div className='relative w-screen overflow-clip h-[100vh]'>
        <img
          alt='bg'
          className='absolute z-0 h-screen w-screen object-cover'
          src='/minigamesgraph/dusk.gif'
        />
        {timer <= 60 && (
          <p className='absolute z-30 text-red-rusty-red'>
            score: {clearCounter - failCounter}
          </p>
        )}
        <img
          className=' absolute w-[75px] h-[130px]'
          alt='konsti'
          src={"/minigamesgraph/konsti.png"}
          style={{
            left: `${playerPosition.x}px`,
            top: `${playerPosition.y}px`,
          }}
        />
        {/* Bolts */}
        {bolts.map((bolt) => (
          <img
            alt='blitz'
            src='/minigamesgraph/blitz.png'
            key={bolt.id}
            className='absolute w-5 h-10 -rotate-90'
            style={{
              left: `${bolt.x}px`,
              top: `${bolt.fromY}px`,
            }}
          />
        ))}
        {/* Bugs */}
        {bugs.map((bug) =>
          bug.isActive ? (
            <img
              alt='käfer'
              src='/minigamesgraph/käfer.png'
              className=' absolute w-24 h-24 -rotate-90'
              key={bug.id}
              style={{
                left: `${bug.x}px`,
                top: `${bug.y}px`,
              }}
            />
          ) : null
        )}
      </div>
    </div>
  );
};

export default BugsNBolts;
