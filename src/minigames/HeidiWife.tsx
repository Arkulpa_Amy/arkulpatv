import React, {useState, useEffect} from "react";

interface Position {
  x: number;
  y: number;
}

interface Stripe {
  id: number;
  x: number;
}

const HeidiWife: React.FC = () => {
  const [playerPosition, setPlayerPosition] = useState<Position>({
    x: 50,
    y: 85,
  });
  const [hiderPosition, setHiderPosition] = useState<Position>({
    x: 100,
    y: 90,
  });

  const [hiddenTimer, setHiddenTimer] = useState<number>(0);
  const [outTimer, setOutTimer] = useState<number>(100);
  const [direction, setDirection] = useState("right");
  const [gameStarted, setGameStarted] = useState<boolean>(false);
  const BGaudio = new Audio(
    "/minigamessound/LMFAO - Sexy and I Know It (8bit Version).mp3"
  );

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === "ArrowLeft") {
        setPlayerPosition((prev) => ({
          ...prev,
          x: Math.max(prev.x - 50, 10),
        }));
      } else if (e.key === "ArrowRight") {
        setPlayerPosition((prev) => ({
          ...prev,
          x: Math.min(prev.x + 50, window.innerWidth - 140),
        }));
      }
    };

    window.addEventListener("keydown", handleKeyDown);
    return () => window.removeEventListener("keydown", handleKeyDown);
  }, [playerPosition]);

  useEffect(() => {
    if (outTimer >= 0) {
      const gameInterval = setInterval(() => {
        setOutTimer(outTimer - 1);
      }, 1000);
      return () => clearInterval(gameInterval);
    }
  }, [outTimer]);

  useEffect(() => {
    const hiderMovementInterval = setInterval(() => {
      setHiderPosition((prev) => {
        let newX;
        const randomDirection = Math.random() < 0.5 ? "left" : "right"; // Randomly choose left or right direction
        if (randomDirection === "right") {
          newX = prev.x + Math.floor(Math.random() * 50 + hiddenTimer); // Randomly move to the right
          if (newX >= window.innerWidth - 42) {
            newX = window.innerWidth - 42;
          }
        } else {
          newX = prev.x - Math.floor(Math.random() * 50 + hiddenTimer); // Randomly move to the left
          if (newX <= 0) {
            newX = 0;
          }
        }
        return {...prev, x: newX};
      });
    }, 200 - hiddenTimer);

    const playerWidth = 32;
    const hiderWidth = 32;

    const playerLeft = playerPosition.x;
    const playerRight = playerPosition.x + playerWidth;
    const hiderLeft = hiderPosition.x;
    const hiderRight = hiderPosition.x + hiderWidth;

    if (playerRight > hiderLeft && playerLeft < hiderRight && outTimer >= 0) {
      setHiddenTimer((prevTimer) => Math.min(prevTimer + 0.5));
    }

    return () => clearInterval(hiderMovementInterval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [direction, hiderPosition, playerPosition.x]);

  const [stripes, setStripes] = useState<Stripe[]>([]);

  useEffect(() => {
    const stripeInterval = setInterval(() => {
      setStripes((prevStripes) => {
        const newStripes = [...prevStripes];
        newStripes.push({id: Date.now(), x: window.innerWidth});
        return newStripes;
      });
    }, 5000);

    const moveStripesInterval = setInterval(() => {
      setStripes((prevStripes) => {
        return prevStripes.map((stripe) => ({
          ...stripe,
          x: stripe.x - 10,
        }));
      });
    }, 100);

    return () => {
      clearInterval(stripeInterval);
      clearInterval(moveStripesInterval);
    };
  }, []);

  const resetGame = () => {
    setHiddenTimer(0);
    setOutTimer(100);
    setPlayerPosition({
      x: 50,
      y: 600,
    });
    setHiderPosition({x: 100, y: 530});
  };

  const handleStartGame = () => {
    BGaudio.play().catch((error) =>
      console.error("Error playing audio: ", error)
    );
    setGameStarted(true);
  };

  return (
    <div className='overflow-clip'>
      {!gameStarted && (
        <div className='flex flex-col justify-center items-center h-screen'>
          <img
            alt='bg'
            src='/minigamesgraph/bgWife.gif'
            className='w-screen h-[100vh] absolute -z-20'
          ></img>
          <div className='bg-zuckerpink p-[10%] text-center'>
            <h1 className='text-4xl font-bold mb-6'>Welcome to HeidiWife!</h1>
            <p className='text-lg mb-4'>Steuerung: → ←</p>
            <p className='text-lg mb-6'>
              Je länger du deine Frau versteckst, desto höher wird dein "Score".
            </p>
            <button
              onClick={handleStartGame}
              className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'
            >
              Start Game
            </button>
          </div>
        </div>
      )}
      {gameStarted && (
        <div>
          {outTimer < 0 && (
            <div className='flex flex-row overflow-clip'>
              <img
                alt='bg'
                src='/minigamesgraph/skyline.gif'
                className='h-[100vh] w-auto scale-x-[-1]'
              ></img>

              <img
                alt='bg'
                src='/minigamesgraph/skyline.gif'
                className='h-[100vh] mr-28 w-auto '
              ></img>
            </div>
          )}
          {outTimer < 0 && (
            <div className='inline-block bg-white from-zuckerpink-dunkel via-pflaume to-zwielichtblau bg-blackel w-[60%] h-[35vh] absolute left-[20%] right-[20%] top-[20%] bg-gradient-to-r'>
              <span className='block h-full w-full text-off-white'></span>
              <img
                alt='waifu'
                src='/minigamesgraph/waifufront.gif'
                className='top-[20%] md:top-[4%] p-4 right-[0%] md:h-full h-[75%] absolute'
              />
              <div>
                <img
                  alt='heidi'
                  src='/minigamesgraph/HeidiWife.png'
                  className='absolute w-[50%] left-[25%] right-[25%] top-[10%]'
                />
                <p className='text-off-white bottom-[10%] left-[26%] absolute'>
                  Score: {Math.max(hiddenTimer)}%
                </p>
              </div>
              <img
                alt='waifu'
                src='/minigamesgraph/waifufront.gif'
                className='top-[20%] md:top-[4%] p-4 md:h-full h-[75%] absolute left-[0%]'
              />
              <p
                className='border-off-white absolute bottom-3 left-[60%] text-off-white text-center p-3 border-2 cursor-pointer'
                onClick={() => resetGame()}
              >
                Nochmal?
              </p>
            </div>
          )}
          {outTimer > 1 && (
            <div>
              <div className='absolute -z-20 w-full overflow-clip'>
                <div className='bg-blackel absolute top-0 h-1/2 w-screen -z-50' />
                {stripes.map((stripe) => (
                  <div
                    key={stripe.id}
                    className='absolute z-30 bottom-[5%] h-5 w-60 bg-off-white overflow-clip'
                    style={{left: `${stripe.x}px`}}
                  />
                ))}
                <img
                  alt='background'
                  src={"/minigamesgraph/bg.gif"}
                  className='h-screen w-screen z-50 object-cover'
                />
                <div className=' bg-asphalt h-28 w-screen absolute z-20 bottom-0'></div>
              </div>
              <div className=''>
                <img
                  alt='Player'
                  src='/minigamesgraph/isy.png'
                  className='absolute w-32 h-32 z-20 bottom-[9%]'
                  style={{
                    left: `${playerPosition.x}px`,
                  }}
                />
                <div className='h-auto bg-red-rusty-red bottom-20'>
                  <img
                    alt='wifey'
                    src={"/minigamesgraph/wifeyRight.gif"}
                    className='absolute h-auto bottom-[10%]'
                    style={{
                      left: `${hiderPosition.x}px`,
                    }}
                  />
                </div>
                <p className='bg-zwielichtblau'>
                  Time left: {outTimer} seconds
                </p>
              </div>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default HeidiWife;
