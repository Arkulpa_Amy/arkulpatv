/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useCallback} from "react";

interface Screen {
  isLit: boolean;
  imgSrc: string;
}

interface Kuchen {
  imgSrc: string;
  x: number;
  y: number;
}

const WhackAScreen = () => {
  const [screens, setScreens] = useState<Screen[]>(
    Array(6).fill({isLit: false, imgSrc: ""})
  );
  const [menu, setMenu] = useState(true);
  const [score, setScore] = useState(0);
  const [kuchens, setKuchens] = useState<Kuchen[]>([]);
  const [domiVisible, setDomiVisible] = useState(false);
  const winaudio = new Audio("/minigamessound/laugh.mp3");

  const imageSourcesScreen = [
    "/minigamesgraph/screen.png",
    "/minigamesgraph/screen2.png",
    "/minigamesgraph/screen3.png",
    "/minigamesgraph/screen4.png",
  ];

  const imageSourcesKuchen = [
    "/minigamesgraph/kuchen.png",
    "/minigamesgraph/kuchen2.png",
    "/minigamesgraph/kuchen3.png",
    "/minigamesgraph/kuchen4.png",
    "/minigamesgraph/kuchen5.png",
  ];

  const lightUpRandomScreen = useCallback(() => {
    const newScreens = screens.map(() => ({isLit: false, imgSrc: ""}));
    const randomIndex = Math.floor(Math.random() * screens.length);
    const randomImageIndex = Math.floor(
      Math.random() * imageSourcesScreen.length
    );
    newScreens[randomIndex] = {
      isLit: true,
      imgSrc: imageSourcesScreen[randomImageIndex],
    };
    setScreens(newScreens);
  }, [screens, imageSourcesScreen]);
  useEffect(() => {
    const interval = setInterval(() => {
      lightUpRandomScreen();
    }, 1500 - score * 75);
    return () => clearInterval(interval);
  }, [score, lightUpRandomScreen]);

  const handleClick = useCallback(
    (index: number) => {
      if (screens[index].isLit) {
        setDomiVisible(true);
        winaudio.play();
        setScore(score + 1);
        setScreens(screens.map(() => ({isLit: false, imgSrc: ""})));

        const newKuchen: Kuchen = {
          imgSrc:
            imageSourcesKuchen[
              Math.floor(Math.random() * imageSourcesKuchen.length)
            ],
          x: Math.random() * (window.innerWidth - 100),
          y: Math.random() * (window.innerHeight - 100),
        };
        setKuchens((kuchens) => [...kuchens, newKuchen]);

        setTimeout(() => setDomiVisible(false), 500);
      }
    },
    [score, screens, imageSourcesKuchen, winaudio]
  );

  return (
    <div
      className=' bg-zuckerpink flex flex-col select-none absolute z-0 h-[100vh] overflow-clip'
      onClick={(e) => e.stopPropagation()}
    >
      <img
        className='opacity-20 w-screen h-screen'
        alt='bg'
        src='/minigamesgraph/bgWhacka.png'
        draggable='false'
      ></img>

      {menu && (
        <div className=''>
          <p className='text-xl w-[30%] text-off-black absolute top-[20%] right-[35%] md:text-3xl left-[35%]'>
            Ein Domi muss tun was ein Domi tun muss. Schnapp dir einen Score von
            20 mittels Mausklick.
          </p>
          <p
            onClick={() => setMenu(!menu)}
            className='p-2 border w-[30%] border-zuckerpink-dunkel text-zuckerpink-dunkel hover:cursor-pointer hover:bg-zuckerpink-dunkel hover:text-zuckerpink absolute text-center left-[35%] md:text-2xl right-[35%] top-[32%]'
          >
            Hast du das Zeug dazu ein Domi zu werden?
          </p>
        </div>
      )}

      {score < 20 && !menu && (
        <div className='absolute md:left-[20%] right-[20%] grid grid-cols-2 gap-x-10 md:grid-cols-3 -gap-10'>
          {screens.map((screen, index) => (
            <div
              key={index}
              className='relative w-[400px] h-[220px] z-30'
              onClick={() => handleClick(index)}
            >
              <img
                alt='Screen Background'
                src='/minigamesgraph/laptop.png'
                className='absolute rounded-md z-10 -left-2'
                draggable='false'
              />
              {screen.isLit && (
                <img
                  alt='Screen Content'
                  src={screen.imgSrc}
                  className='absolute bg-zuckerpink w-[160px] h-[100px] top-9 z-20 right-[44.5%] rounded-md animate-pulse'
                  draggable='false'
                />
              )}
            </div>
          ))}
        </div>
      )}
      {kuchens.map((kuchen, index) => (
        <img
          draggable='false'
          key={index}
          src={kuchen.imgSrc}
          alt='Kuchen'
          className='absolute z-10'
          style={{
            left: `${kuchen.x}px`,
            top: `${kuchen.y}px`,
            width: "100px",
            height: "100px",
          }}
        />
      ))}
      {score < 20 && !menu && (
        <div className='absolute md:right-[45%] right-[20%] bottom-10 z-50 bg-zuckerpink mt-4 text-4xl text-zuckerpink-dunkel border border-zuckerpink-dunkel place-self-center p-1'>
          Score: {score}
        </div>
      )}
      {score >= 20 && (
        <div className='w-fit bg-zuckerpink'>
          <img
            draggable='false'
            alt='trolltanz'
            src='/minigamesgraph/troll-dancing.gif'
            className='absolute z-50 md:w-[24%] w-[70%] bottom-[20%] md:top-20 left-[25%] right-[15%] md:left-[43%] md:right-[43%]'
          />
          <p className='text-zuckerpink text-3xl bg-zuckerpink-dunkel p-10 absolute pr-5 left-[5%] w-[90%] right-[5%] z-50 bottom-[5%] md:text-9xl'>
            Dominik wins. Fatality
            <button
              className='text-zuckerpink absolute right-[4%] bottom-[25%] border ml-10 mt-10 text-2xl border-zuckerpink p-2'
              type='button'
              onClick={() => setScore(0)}
            >
              Again?
            </button>
          </p>
        </div>
      )}
      <div className='bg-zuckerpink h-auto w-screen'>
        {domiVisible && (
          <img
            alt='domi'
            src='/minigamesgraph/domi.png'
            className='absolute w-[25%] bottom-0 md:bottom-28 left-[12%] h-auto md:w-[11%] right-[35%] md:left-[45%] md:h-52 z-20'
          />
        )}
      </div>
    </div>
  );
};

export default WhackAScreen;
